/*jslint browser: true, plusplus:true*/

describe("scenario", function () {

    it("assertion", function () {
        expect(1).toBe(1);
    });

});


describe("html tests", function () {

    beforeEach(function () {
        app.init();
    });

    it("Should verify that DOM elements are created for the container and the title", function () {
        var expectedValue = 'Real Time Stockquote App';
        var actualValue = app.initHTML().querySelector("h1").innerText;

        expect(actualValue).toBe(expectedValue);

    });

    it("Should verify that the table has 25 rows", function () {
        var actualValue = app.showData().querySelectorAll("tr").length;
        expect(actualValue).toBe(25)
    });


    it("Should verify that only letters and numbers remain", function () {
        var actualValue = app.createValidCSSNameFromCompany("HGF>&^#%%$#@");
        var expectedValue = "HGF";
        expect(actualValue).toBe(expectedValue);
    });

});