(function () {
    window.app = {

        series: {},

        socket: io("http://server7.tezzt.nl:1333"),

        settings: {
            refresh: 1000,
            ajaxUrl: "http://server7.tezzt.nl/~theotheu/stockquotes/index.php"
        },

        rnd: function (min, max) {
            // return a value between min and max (including the min and max values),
            // eg. if the current value is 100,
            // then the new value will be a float between
            // 100-min and 100+max
        },

        getFormattedDate: function (d) {
            // Return a formatted date string that looks like
            // month/day/year
            // year is 4 digits
        },

        getFormattedTime: function (d) {
            // am, pm
        },

        generateTestData: function () {
            var company, quote, newQuote;

            for (company in app.series) {
                quote = app.series[company][0];
                newQuote = Object.create(quote);
                newQuote.col1 = Math.random() * 100; // new value, should be calculated with rnd
                newQuote.col2 = new Date(); // new date
                newQuote.col3 = new Date(); // new time, including am, pm
                newQuote.col4 = -1 + Math.floor(Math.random() * 3); // difference of price value between this quote and the previous quote

                app.series[company].push(newQuote);

            }
        },

        parseData: function (rows) {
            var i, company, propertyName, propertyValue;

            // Iterate over the rows and add to series
            for (i = 0; i < rows.length; i++) {
                company = rows[i].col0;

                // Check if array for company exist in series
                if (app.series[company] !== undefined) {
                    app.series[company].push(rows[i]);
                } else {
                    // company does not yet exist
                    app.series[company] = [rows[i]];
                }
            }

        },

        retrieveData: function () {

        },

        createValidCSSNameFromCompany: function (str) {
            // regular expression to remove everything
            // that is not out of A-z0-9
            return str.replace(/\W/, "");
        },

        showData: function () {
            // return value is a dom
            var table, company, row, quote, cell, propertyName, propertyValue;

            // Create table
            table = document.createElement("table");

            // Create header
            // TODO: you

            // Create rows
            for (company in app.series) {
                quote = app.series[company][0];
                row = document.createElement("tr");
                row.id = app.createValidCSSNameFromCompany(company);

                // Create cells
                table.appendChild(row);

                // Iterate over quote to create cells
                for (propertyName in quote) {
                    propertyValue = quote[propertyName];
                    cell = document.createElement("td");
                    cell.innerText = propertyValue;
                    row.appendChild(cell);
                }

                if (quote.col4 < 0) {
                    row.className = "loser";
                } else if (quote.col4 > 0) {
                    row.className = "winner";
                }
            }

            return table;

        },

        getRealtimeData: function () {
            app.socket.on('stockquotes', function (data) {
                // TODO: integrate this in your code
                console.log(data);
            });
        },

        loop: function () {
            var table;

            // app.generateTestData();

            app.getRealtimeData();

            // remove old table
            document.querySelector("#container")
                .removeChild(
                document.querySelector("table")
            );

            // add new table
            table = app.showData();
            app.container.appendChild(table);

            setTimeout(app.loop, app.settings.refresh)

        },

        initHTML: function () {
            var container, h1Node;

            // Create container
            container = document.createElement("div");
            container.id = "container";

            app.container = container;


            // Create title of application
            h1Node = document.createElement("h1");
            h1Node.innerText = "Real Time Stockquote App";

            app.container.appendChild(h1Node);

            return app.container;

        },

        init: function () {

            var container, table;

            // Add HTML to page
            container = app.initHTML();
            document.querySelector("body").appendChild(container);


            // Parse initial data
            app.parseData(data.query.results.row);

            table = app.showData();
            app.container.appendChild(table);


        }

    }
}());